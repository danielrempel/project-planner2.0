#### PROJECT SETTINGS ####
# The name of the executable to be created
BIN_NAME := $(notdir $(PWD))
# Compiler used
CXX ?= g++
# Extension of source files used in the project
SRC_EXT = cpp
# Path to the source directory, relative to the makefile
SRC_PATH = src
# Space-separated pkg-config libraries used by this project
LIBS = gtkmm-3.0 sqlite3
# General compiler flags
COMPILE_FLAGS = -std=c++14 -Wall -Wextra -g
# Additional release-specific flags
RCOMPILE_FLAGS =
# Additional debug-specific flags
DCOMPILE_FLAGS = -D DEBUG -D DEBUG_PATHS
# Add additional include paths
INCLUDES = -I $(SRC_PATH)
# General linker settings
LINK_FLAGS = -Lext -linih -lpthread
# Additional release-specific linker settings
RLINK_FLAGS =
# Additional debug-specific linker settings
DLINK_FLAGS =
# Destination directory, like a jail or mounted system
DESTDIR = /
# Install path (bin/ is appended automatically)
INSTALL_PREFIX = usr/local
#### END PROJECT SETTINGS ####

debug: inih
release: inih
clean: inih-clean

inih:
	@make CMD_PREFIX=${CMD_PREFIX} -f ../inih.mk -C ext/ libinih.a
inih-clean:
	@make CMD_PREFIX=${CMD_PREFIX} -f ../inih.mk -C ext/ clean
run: debug
	@echo "Running ${BIN_NAME}:"
	@echo ==============================================================
	@cd rundir; ./${BIN_NAME}
	@echo ==============================================================

gdb: debug
	@echo "GDB ${BIN_NAME}:"
	@echo ==============================================================
	@cd rundir; gdb ./${BIN_NAME}
	@echo ==============================================================
