#ifndef __HAVE__NOTESPAGE_HPP__
#define __HAVE__NOTESPAGE_HPP__

class NotesPage;

#include <gtkmm.h>
#include <vector>
#include <glibmm/ustring.h>

#include "NotesBlock.hpp"

class NotesPage : public Gtk::Container {
	protected:
		Gtk::Entry *entryNoteName;
		Gtk::Button *buttonRemoveNote;
		Gtk::Button *buttonSaveNote;
		Gtk::TextView *textViewNoteText;
		Gtk::Label *pageLabel;
		
		Glib::ustring pageName;
		
		NotesBlock *parent;
		
		void updatePageName();
	public:
		NotesPage(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);
		~NotesPage();
		
		// vv called from ActionAddPage (defined in NotesBlock.cpp)
		void initPage(NotesBlock *parent, Glib::ustring pagename);
		
		void onRemoveNoteClicked();
		void onSaveNoteClicked();
		
		Gtk::Label *getLabel();
		
		void onPageTextLoaded(void *agltStruct);
		
		void notifyAboutExit();
};

#endif
