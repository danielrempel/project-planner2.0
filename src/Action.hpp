#ifndef __HAVE_ACTION_HPP__
#define __HAVE_ACTION_HPP__
class Action;

class Action
{

	public:
		virtual void execute() = 0;
	protected:
	private:
};

#endif // __HAVE_ACTION_HPP__
