#ifndef __HAVE_ACTIONQUEUE_HPP__
#define __HAVE_ACTIONQUEUE_HPP__
class ActionQueue;

#include "Action.hpp"
#include <queue>
#include <mutex>
#include <condition_variable>
#include <thread>

// Creates a thread on start
// Guarantees that all the Actions
// in the queue will be executed before
// thread ends after AQ::stop

class ActionQueue
{
	public: // Singleton stuff
		static ActionQueue& getInstance();
		ActionQueue(ActionQueue const&)			= delete;
		void operator=(ActionQueue const&)		= delete;

	public:
		ActionQueue();
		virtual ~ActionQueue();
		
		void addAction(Action* a);
		void run();
		void stop();
		
		void join();
	protected:
	private:
		std::queue<Action*> queue;
		char running;
		std::mutex runningMutex;
		std::mutex queueMutex;
		
		std::mutex cvMutex;
		std::condition_variable cv;
		
		std::thread ownThread;
};

/*
 * Test code:

	include <unistd.h> // sleep(int)

	ActionQueue::getInstance();
	
	ActionQueue::getInstance().addAction(new EchoAction("meow1"));
	ActionQueue::getInstance().addAction(new EchoAction("meow2"));
	
	sleep(5);
	
	ActionQueue::getInstance().addAction(new EchoAction("meow3"));
	ActionQueue::getInstance().addAction(new EchoAction("meow4"));
	
	sleep(3);
	
	ActionQueue::getInstance().addAction(new EchoAction("meow5"));
	
	ActionQueue::getInstance().stop();
	
	ActionQueue::getInstance().join();*/

#endif // __HAVE_ACTIONQUEUE_HPP__
