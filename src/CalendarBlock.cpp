#include "CalendarBlock.hpp"
#include "../ext/StreamProxy.hpp"
#include "Action.hpp"
#include "ActionQueue.hpp"
#include "Date.hpp"
#include "debug.hpp"
#include "DataProvider.hpp"
#include <string>
#include <glib.h>
#include <memory>
#include <stdlib.h>

class ActionGetDailyNotes : public Action
{
	private:
		CalendarBlock *caller;
		Date date;
	public:
		struct AGDNStruct
		{
			Date *date;
			std::string *note;
			CalendarBlock *caller;
		};
		ActionGetDailyNotes(CalendarBlock *caller, Date date)
			: caller(caller), date(date)
		{}
		virtual void execute()
		{
			struct AGDNStruct *params = (struct AGDNStruct *)malloc(sizeof(struct AGDNStruct)); // freed in CalendarBlock::onDailyNoteLoaded()
			params->date = new Date(date);
			params->note = new std::string(DataProvider::getInstance().getDailyNote(date));;
			params->caller = caller;
			g_main_context_invoke(NULL,
				[](void* params)->int
				{
					struct ActionGetDailyNotes::AGDNStruct *p = (struct ActionGetDailyNotes::AGDNStruct *)params;
					p->caller->onDailyNoteLoaded(params);
					return 0;
				},
				(void*)params);
		}
};

class ActionSaveDailyNotes : public Action
{
	private:
		Date date;
		std::string note;
	public:
		ActionSaveDailyNotes(Date date, std::string note)
			: date(date), note(note)
		{}
		virtual void execute()
		{
			DataProvider::getInstance().saveDailyNote(date, note);
			DEBUG_OUT("note " << (std::string)date << " saved");
		}
};

// @param ActionGetDailyNotes's parameter struct
void
CalendarBlock::onDailyNoteLoaded(void* agdnStruct)
{
	struct ActionGetDailyNotes::AGDNStruct *params = (struct ActionGetDailyNotes::AGDNStruct *)agdnStruct;
	for(int i=0; i<7; i+=1)
	{
		if(weekdays[i].date == (*(params->date)))
		{
			weekdays[i].textView->get_buffer()->set_text(Glib::ustring(*(params->note)));
			weekdays[i].loadedNote = Glib::ustring(*(params->note));
			break;
		}
	}
	delete params->note;
	delete params->date;
	free(agdnStruct);
}

CalendarBlock::CalendarBlock
	(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade)
	 : Gtk::Container(cobject), builder(refGlade)
{
	initWeekdays();
	linkButtons();

	// Get current week's first day
	// TODO: make first weekday configurable
	Date today;
	
	if(firstDayOfWeek == today.getWeekday())
	{
		thisWeeksFirstDay = today;
	}
	else
	{
		int daysTillFirstDay = today.getWeekday() - (firstDayOfWeek % 7);
		thisWeeksFirstDay = today - daysTillFirstDay;
	}
	weekReinit(true);
}

CalendarBlock::~CalendarBlock()
{
}

void
CalendarBlock::notifyAboutExit()
{
	DEBUG_OUT("notified about exit; saving notes");
	saveWeek();
}

// =====================================================================
// GTK Initialization / mandatory variable linking code
void
CalendarBlock::initWeekdays()
{
	for(int i=1; i<8; i+=1)
	{
		// Acquiring widgets, setting their properties
		builder->get_widget
			(
				(std::string)(StreamProxy() << "labelweekday" << i),
				weekdays[i-1].label
			);
		builder->get_widget
			(
				(std::string)(StreamProxy() << "textweekday" << i),
				weekdays[i-1].textView
			);
	}
}

void
CalendarBlock::linkButtons()
{
	builder->get_widget("buttonPrevWeek", buttonPrevWeek);
	builder->get_widget("buttonNextWeek", buttonNextWeek);
	buttonPrevWeek->signal_clicked().connect(sigc::mem_fun(*this, &CalendarBlock::onPrevWeekClicked));
	buttonNextWeek->signal_clicked().connect(sigc::mem_fun(*this, &CalendarBlock::onNextWeekClicked));
}
// =====================================================================

// =====================================================================
// Week reinitialisation: save data, load data
void
CalendarBlock::weekReinit(char isInit)
{
	if(!isInit) // shouldn't save if called from CalendarBlock()
		{saveWeek();}
	for(int i=0; i<7; i+=1)
	{
		weekdays[i].date = thisWeeksFirstDay + i;
		weekdays[i].label->set_text(weekdays[i].date);
		ActionQueue::getInstance().addAction
		(
			new ActionGetDailyNotes(this, Date(weekdays[i].date))
		);
	}
}

// =====================================================================
// Data procedures
void
CalendarBlock::saveWeek()
{
	for(int i=0; i<7; i+=1)
	{
		saveWeekday(weekdays[i].date);
	}
}

void
CalendarBlock::saveWeekday(Date d)
{
	int i=0;
	while(( !(d == weekdays[i].date) )&&(i<7))
	{ i+=1; }
	
	if(!(d == weekdays[i].date))
	{
		DEBUG_OUT("Wow! Didn't find the requested weekday");
		return;
	}

	if(0!=weekdays[i].loadedNote.compare
		(weekdays[i].textView->get_buffer()->get_text())
		)
	{
		ActionQueue::getInstance().addAction
		(
			new ActionSaveDailyNotes
			(
			weekdays[i].date,
			weekdays[i].textView->get_buffer()->get_text().raw()
			)
		);
		weekdays[i].loadedNote =
				weekdays[i].textView->get_buffer()->get_text();
	}
}

// =====================================================================
// Date management
void
CalendarBlock::onPrevWeekClicked()
{
	thisWeeksFirstDay = thisWeeksFirstDay - 7;
	weekReinit();
}

void
CalendarBlock::onNextWeekClicked()
{
	thisWeeksFirstDay = thisWeeksFirstDay + 7;
	weekReinit();
}
