#ifndef __HAVE__VERSION_H__
#define __HAVE__VERSION_H__

struct{

	const char* app_name = "Planner";
	int v_maj = VERSION_MAJOR;
	int v_min = VERSION_MINOR;
	int v_pat = VERSION_PATCH;
	int v_rev = VERSION_REVISION;
	const char* v_hash = VERSION_HASH;

} ApplicationInfo;

#endif
