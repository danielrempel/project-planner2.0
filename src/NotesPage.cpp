#include "NotesPage.hpp"
#include "DataProvider.hpp"
#include "ActionQueue.hpp"
#include "Action.hpp"

#include "debug.hpp"

#include <glib.h>
#include <stdlib.h>

class ActionGetListText : public Action
{
	private:
		std::string pageName;
		NotesPage* caller;
	public:
		struct AGLTStruct
			{
				std::string *val;
				NotesPage *caller;
			};
		ActionGetListText(NotesPage* caller, std::string pageName)
			: caller(caller), pageName(pageName)
			{}
		virtual void execute()
		{
			struct AGLTStruct *params = (struct AGLTStruct *)malloc(sizeof(struct AGLTStruct));
			params->val = new std::string(DataProvider::getInstance().getList(pageName));
			params->caller = caller;
			g_main_context_invoke(NULL,
				[](void* params)->int
				{
					ActionGetListText::AGLTStruct *p = (ActionGetListText::AGLTStruct*)params;
					p->caller->onPageTextLoaded(params);
					return 0;
				},
				(void*)params);
		}
};

class ActionSaveList : public Action
{
	private:
		std::string listName;
		std::string listText;
	public:
		ActionSaveList(std::string listName, std::string listText)
			: listName(listName), listText(listText)
		{}
		virtual void execute()
		{
			DEBUG_OUT("saving list " << listName);
			DataProvider::getInstance().saveList(listName, listText);
		}
};

class ActionRemoveList : public Action
{
	private:
		std::string listName;
	public:
		ActionRemoveList(std::string listName)
			: listName(listName)
		{};
		virtual void execute()
		{
			DEBUG_OUT("removing list " << listName);
			DataProvider::getInstance().removeList(listName);
		}
};

NotesPage::NotesPage
	(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade)
	: Gtk::Container(cobject)
{
	refGlade->get_widget("entryNoteName", entryNoteName);
	refGlade->get_widget("buttonRemoveNote", buttonRemoveNote);
	refGlade->get_widget("buttonSaveNote", buttonSaveNote);
	refGlade->get_widget("textViewNoteText", textViewNoteText);
	
	buttonRemoveNote->signal_clicked().connect(sigc::mem_fun(*this, &NotesPage::onRemoveNoteClicked));
	buttonSaveNote->signal_clicked().connect(sigc::mem_fun(*this, &NotesPage::onSaveNoteClicked));

	this->pageLabel = new Gtk::Label();
}

NotesPage::~NotesPage()
{
}

void
NotesPage::notifyAboutExit()
{
	DEBUG_OUT("Saving note: '" << (std::string)(entryNoteName->get_buffer()->get_text()) << "'");
	if( pageName.compare(entryNoteName->get_buffer()->get_text()) != 0 )
	{
		// the name was changed:
		ActionQueue::getInstance().addAction
		(
			new ActionRemoveList
				(
					pageName.raw()
				)
		);
	}
	ActionQueue::getInstance().addAction
		(
			new ActionSaveList
				(
					pageName.raw(),
					textViewNoteText->get_buffer()->get_text().raw()
				)
		);
}

void
NotesPage::initPage(NotesBlock *parent, Glib::ustring pagename)
{
	this->parent = parent;
	this->pageName = pagename;
	this->pageLabel->set_text(this->pageName);
	entryNoteName->get_buffer()->set_text(pageName);
	
	ActionQueue::getInstance().addAction(
			new ActionGetListText(this, pageName.raw())
		);
}

void
NotesPage::onPageTextLoaded(void *agltStruct)
{
	ActionGetListText::AGLTStruct *params = (ActionGetListText::AGLTStruct*)agltStruct;
	std::string *pageText = params->val;
	textViewNoteText -> get_buffer() -> set_text(Glib::ustring(*pageText));
	delete pageText;
	free(agltStruct);
}

void
NotesPage::updatePageName()
{
	pageName = Glib::ustring(entryNoteName->get_buffer()->get_text());
	pageLabel->set_text(pageName);
}

void
NotesPage::onRemoveNoteClicked()
{
	ActionQueue::getInstance().addAction
		(
			new ActionRemoveList
				(
					pageName.raw()
				)
		);
	parent->removePage(this);
}

void
NotesPage::onSaveNoteClicked()
{
	if( pageName.compare(entryNoteName->get_buffer()->get_text()) != 0 ) {
		// the name was changed:
		ActionQueue::getInstance().addAction
		(
			new ActionRemoveList
				(
					pageName.raw()
				)
		);
		updatePageName();
	}
	ActionQueue::getInstance().addAction
		(
			new ActionSaveList
				(
					pageName.raw(),
					textViewNoteText->get_buffer()->get_text().raw()
				)
		);
	updatePageName();
}

Gtk::Label*
NotesPage::getLabel()
{
	return this->pageLabel;
}
