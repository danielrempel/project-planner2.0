#include "NotesBlock.hpp"
#include "SettingsProvider.hpp"
#include "DataProvider.hpp"

#include "debug.hpp"

NotesBlock::NotesBlock
	(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade)
	: Gtk::Notebook(cobject), pageForRemoval(NULL)
{	
	refGlade->get_widget("buttonAddTab", buttonAddTab);
	
	buttonAddTab->signal_clicked().connect
		(
			sigc::mem_fun(*this, &NotesBlock::onAddTabClicked)
		);
	this->signal_switch_page().connect
		(
			sigc::mem_fun(*this, &NotesBlock::onSwitchPage)
		);
	
	std::vector<std::string> tabs = DataProvider::getInstance().getListNames();
	// this may be the only place where I will allow direct use of
	// the dataProvider
	// otherwise too much logic may be destroyed
	// or not..?
	// ..couldn't replace with Action : couldn't pass vector to lambda etc
	for (auto & pageName : tabs) {
		addPage(pageName);
	}
}

void
NotesBlock::notifyAboutExit()
{
	// for each page
	// page->notifyAboutExit()
	int index = 0;
	NotesPage* page;
	while(
		nullptr != (page = dynamic_cast<NotesPage*>(this->get_nth_page(index)))
	)
	{
		DEBUG_OUT("Page: " << index);
		page->notifyAboutExit();
		index+=1;
	}
}

// =====================================================================
// Signal handling:

void
NotesBlock::onSwitchPage(Gtk::Widget* page, guint page_number)
{
	DEBUG_OUT("page number: " << page_number);
	
	// vv this code forbids user from entering the buttonAddTab page
	if(this->get_n_pages() -1 == page_number)
	{
		this->prev_page();
	}
	
	// page removal logic
	if(NULL != pageForRemoval) {
		this->remove_page(*pageForRemoval);
		pageForRemoval = NULL;
		// the pages get destroyed somehow. Supposedly the GTK itself
		// tracks unused objects
	}
}

void
NotesBlock::onAddTabClicked()
{
	addPage();
}
// =====================================================================
// =====================================================================

// =====================================================================
// Page manipulation:

void
NotesBlock::addPage(std::string pageName)
{
	NotesPage *page = 0;
	Glib::RefPtr<Gtk::Builder> builder =
				Gtk::Builder::create_from_file( SettingsProvider::getInstance().getParam("uifile").c_str() );
	builder->get_widget_derived("singleNoteBlock", page);
	page->initPage(this, pageName);
	prepend_page(*page, *page->getLabel());
	set_current_page(0);
	DEBUG_OUT("added page '" << pageName << "'");
}

void
NotesBlock::removePage(NotesPage *page)
{
	pageForRemoval = page;
	if(this->get_n_pages() == 2) {
		addPage();
	}
	if(this->page_num(*page) > 0) {
		this->prev_page();
	} else {
		this->next_page();
	}
}
// =====================================================================
// =====================================================================
