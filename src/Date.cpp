#include "Date.hpp"

#include "../ext/StreamProxy.hpp"
#include "debug.hpp"

Date::Date(sys_days date)// : date(date)
{
	this->date = sys_days(date);
}

Date::Date() : date( floor<days>(std::chrono::system_clock::now()) )
{}

Date::Date(const Date& date) : date(date.getSysDays())
{}

Date::~Date()
{}

int
Date::getStorageDate()
{
	auto ymd = year_month_day{date};
	return static_cast<int>(ymd.year())*10000 + static_cast<unsigned>(ymd.month())*100 + static_cast<unsigned>(ymd.day());
}

sys_days
Date::getSysDays() const
{
	return sys_days(date);
}

Date
Date::operator+(const days& d)
{
	return Date(date + d);
}

Date
Date::operator+(const int& b)
{
	return Date(date + days{b});
}
Date
Date::operator-(const int& b)
{
	return Date(date - days{b});
}

unsigned int
Date::getWeekday()
{
	return (unsigned)(( year_weeknum_weekday{ date } ).weekday());
}

std::string
Date::getString()
{
	//return std::string(StreamProxy() << date::weekday{date} <<"\n" << year_month_day{date});
	return std::string(StreamProxy() << date::weekday{date} << "\n" << year_month_day{date});
}

Glib::ustring
Date::getUString()
{
	//return Glib::ustring(StreamProxy() << date::weekday{date} <<"\n" << year_month_day{date});
	return Glib::ustring(this->getString());
}

Date::operator std::string()
{
	return getString();
}

Date::operator Glib::ustring()
{
	return getUString();
}

bool
Date::operator==(const Date &b) const
{
	return (this->getSysDays() == b.getSysDays());
}
/*
#include "../ext/StreamProxy.hpp"

std::string
Date::describe() const
{
	return std::string(StreamProxy()<<(uint64_t)&(this->date));
}
*/
