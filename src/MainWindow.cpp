#include "MainWindow.hpp"
#include "debug.hpp"

MainWindow::MainWindow
	(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade)
	 : Gtk::Window(cobject), builder(refGlade)
{
	builder->get_widget("buttonSettings", buttonSettings);
	builder->get_widget("buttonCalendar", buttonCalendar);
	builder->get_widget("buttonNotes", buttonNotes);
	builder->get_widget("paned1", paned);

	builder->get_widget_derived("calendarBlock", calendarBlock);
	builder->get_widget_derived("notesBlock", notesBlock);

	paned->add2(*calendarBlock);

	buttonCalendar->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::onCalendarButtonClicked));
	buttonNotes->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::onNotesButtonClicked));
	
	this->signal_delete_event().connect(sigc::mem_fun(this,&MainWindow::onExitClicked));
}

MainWindow::~MainWindow()
{
	delete calendarBlock;
	delete notesBlock;
}

void
MainWindow::onCalendarButtonClicked()
{
	DEBUG_OUT("Turn into calendar!");
	if(paned->get_child2() == notesBlock) {
		paned->remove(*notesBlock);
		paned->add2(*calendarBlock);
	}
}

void
MainWindow::onNotesButtonClicked()
{
	DEBUG_OUT("Turn into notepad!");
	if(paned->get_child2() == calendarBlock) {
		paned->remove(*calendarBlock);
		paned->add2(*notesBlock);
	}
}

bool
MainWindow::onExitClicked(GdkEventAny* event)
{
	DEBUG_OUT("onExitClicked()");
	calendarBlock->notifyAboutExit();
	notesBlock->notifyAboutExit();
	return false; // propagate the event
}
