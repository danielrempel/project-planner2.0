#ifndef __HAVE_MAINWINDOW_HPP__
#define __HAVE_MAINWINDOW_HPP__
class MainWindow;

#include <gtkmm.h>

#include "CalendarBlock.hpp"
#include "NotesBlock.hpp"

class MainWindow : public Gtk::Window {
	protected:
		Glib::RefPtr<Gtk::Builder> builder;
		Gtk::Button *buttonSettings;
		Gtk::Button *buttonCalendar;
		Gtk::Button *buttonNotes;
		Gtk::Paned *paned;
		CalendarBlock *calendarBlock;
		NotesBlock *notesBlock;
		
	public:
		MainWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);
		~MainWindow();
	protected:
		void onCalendarButtonClicked();
		void onNotesButtonClicked();
		bool onExitClicked(GdkEventAny* event);
};

#endif // __HAVE_MAINWINDOW_HPP__
