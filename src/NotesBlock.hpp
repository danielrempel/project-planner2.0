#ifndef __HAVE_NOTESBLOCK_HPP__
#define __HAVE_NOTESBLOCK_HPP__

class NotesBlock;

#include <gtkmm.h>
#include <vector>
#include <glibmm/ustring.h>

#include "NotesPage.hpp"

class NotesBlock : public Gtk::Notebook
{
	protected:
		Gtk::Button *buttonAddTab;
		
		NotesPage* pageForRemoval;

	public:
		NotesBlock(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);
		
		void onSwitchPage(Gtk::Widget* page, guint page_number);
		void onAddTabClicked();
		
		void addPage(std::string pageName = std::string("new page"));
		void removePage(NotesPage *page);
		// Is called from the page itself
		// Page removal logic:
		// NotesPage calls NotesBlock::removePage
		// NotesBlock::removePage stores pageForRemoval and switches page
		// NotesBlock::onSwitchPage is called						^^^
		// NotesBlock::onSwitchPage executes switching pages and
		// 				handles page removal
		
		void notifyAboutExit();
};

#endif
