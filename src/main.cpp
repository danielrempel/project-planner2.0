#include "SettingsProvider.hpp"
#include "DataProvider.hpp"
#include "Date.hpp"

#include "debug.hpp"

#include <gtkmm.h>
#include "MainWindow.hpp"

#include "ActionQueue.hpp"
#include "Action.hpp"

#include "version.h"

class EchoAction : public Action
{
	public:
		EchoAction(const char* msg) : msg(msg)
		{}
		virtual void execute()
		{
			DEBUG_OUT(msg);
		}
	private:
		const char * msg;
};

int main(int argc, char** argv)
{
	try
	{

		ActionQueue::getInstance();
		SettingsProvider::getInstance().loadSettings();
		DataProvider::getInstance().init();
		
		auto app =
				Gtk::Application::create(argc, argv);

		Glib::RefPtr<Gtk::Builder> builder =
				Gtk::Builder::create_from_file( SettingsProvider::getInstance().getParam("uifile").c_str() );

		MainWindow *mw = 0;
		builder->get_widget_derived("mainWindow", mw);

		mw->property_title() = (Glib::ustring)
			std::string(StreamProxy() <<
					ApplicationInfo.app_name <<
					" v" << ApplicationInfo.v_maj <<
					'.' << ApplicationInfo.v_min <<
					'.' << ApplicationInfo.v_pat <<
					'-' << ApplicationInfo.v_rev <<
					'-' << ApplicationInfo.v_hash
					);

		app->run(*mw);
		delete mw;
	}
	catch(std::string exc)
	{
		DEBUG_OUT("Exception: " << exc);
	}
	catch(Glib::FileError fe)
	{
		DEBUG_OUT("Exception: " << (std::string)fe.what());
	}
	
	ActionQueue::getInstance().stop();
	ActionQueue::getInstance().join();
}
