#include "DataProvider.hpp"
#include "SettingsProvider.hpp"
#include <iostream>

#include "../ext/cpp/INIReader.h"
#include "../ext/StreamProxy.hpp"

#define EXCEPTION_STR(X) \
	std::string(StreamProxy() << __PRETTY_FUNCTION__ << ": " << X)

#include "debug.hpp"

#include <sqlite3.h>

DataProvider& DataProvider::getInstance()
{
	static DataProvider instance; // initialized only once
	return instance;
}

DataProvider::DataProvider()
{
}

DataProvider::~DataProvider()
{
	sqlite3_close(db);
}

void
DataProvider::init() throw (std::string)
{
#ifdef DEBUG
	DEBUG_OUT("database: '" << SettingsProvider::getInstance().getParam("database") << "'");
#endif
	int err = sqlite3_open(
			SettingsProvider::getInstance().getParam("database").c_str(),
			&db);
	if(err) {
		DEBUG_OUT("Failed to open the database: " << sqlite3_errmsg(db));
		sqlite3_close(db);
		throw(EXCEPTION_STR("sqlite3 error: " << sqlite3_errmsg(db)));
	}

	checkInitDB();

}

void
DataProvider::checkInitDB() throw(const char*) {
	char* errmsg = NULL;
	// TODO: some error protection
	sqlite3_exec(db, "create table if not exists \"DailyNotes\" ("
			"    \"date\" INTEGER PRIMARY KEY NOT NULL,"
			"    \"note\" TEXT NOT NULL);", NULL, NULL, &errmsg);
	if(NULL != errmsg) {
		DEBUG_OUT("DailyNotes table check failed: " << errmsg);
		//sqlite3_free(errmsg);
		//errmsg = NULL;
		throw errmsg;
	}
	/* Daily notes:
	create table if not exists "DailyNotes" (
    "date" INTEGER PRIMARY KEY NOT NULL,
    "note" TEXT NOT NULL
	);
	 */
	sqlite3_exec(db, "create table if not exists \"Lists\" (\"name\" TEXT PRIMARY KEY NOT NULL,"
			"\"note\" TEXT NOT NULL);", NULL, NULL, &errmsg);
	if(NULL != errmsg) {
		DEBUG_OUT("Lists table check failed: " << errmsg);
		//sqlite3_free(errmsg);
		//errmsg = NULL;
		throw(EXCEPTION_STR("sqlite3 error: " << errmsg));
	}
	/* Lists:
	create table if not exists "Lists" (
    "name" TEXT NOT NULL,
    "note" TEXT NOT NULL
	 */
}
