#include <sqlite3.h>
#include "DataProvider.hpp"

#include "../ext/cpp/INIReader.h"
#include "../ext/StreamProxy.hpp"

#define EXCEPTION_STR(X) \
	std::string(StreamProxy() << __PRETTY_FUNCTION__ << ": " << X)

#include "debug.hpp"

void
DataProvider::saveList(std::string const &listName, std::string const &listText) {
	std::string query = "insert or replace into Lists(name, note) values($1,$2);";
	sqlite3_stmt *stmt;
	const char *pzTail;
	int errcode = sqlite3_prepare(db, query.c_str(), query.length(), &stmt, &pzTail);
	if(SQLITE_OK != errcode) {
		DEBUG_OUT("Failed to prepare statement: " << sqlite3_errstr(errcode));
		return;
	}
	errcode = sqlite3_bind_text(stmt, 1, listName.c_str(), listName.length(), SQLITE_STATIC);
	if(SQLITE_OK != errcode) {
		DEBUG_OUT("Failed to bind text to statement: " << sqlite3_errstr(errcode));
		return;
	}
	errcode = sqlite3_bind_text(stmt, 2, listText.c_str(), listText.length(), SQLITE_STATIC);
	if(SQLITE_OK != errcode) {
		DEBUG_OUT("Failed to bind text to statement: " << sqlite3_errstr(errcode));
		return;
	}
	errcode = sqlite3_step(stmt);
	if(SQLITE_DONE != errcode) {
		DEBUG_OUT("sqlite3_step failed: " << sqlite3_errstr(errcode));
		return;
	}
	sqlite3_finalize(stmt);
}

std::string
DataProvider::getList(std::string const &listName) {
	std::string note = "";
	std::string query = "select note from Lists where name = $1;";
	sqlite3_stmt *stmt;
	const char *pzTail;
	int errcode = sqlite3_prepare(db, query.c_str(), query.length(), &stmt, &pzTail);
	if(SQLITE_OK != errcode) {
		DEBUG_OUT("Failed to prepare statement: " << sqlite3_errstr(errcode));
		return std::string();
	}
	errcode = sqlite3_bind_text(stmt, 1, listName.c_str(), listName.length(), SQLITE_STATIC);
	if(SQLITE_OK != errcode) {
		DEBUG_OUT("Failed to bind text to statement: " << sqlite3_errstr(errcode));
		return std::string();
	}
	errcode = sqlite3_step(stmt);
	if((SQLITE_DONE != errcode)&&(SQLITE_ROW != errcode)) {
		DEBUG_OUT("sqlite3_step failed: " << sqlite3_errstr(errcode) << " / " << errcode);
		return std::string();
	}
	if(SQLITE_TEXT == sqlite3_column_type(stmt, 0)) {
		note = std::string( reinterpret_cast<const char*>(sqlite3_column_text(stmt, 0)) );
	} else {
		// there's no note
		return std::string();
	}
	sqlite3_finalize(stmt);
	return note;
}

std::vector<std::string>
DataProvider::getListNames() {
	
	std::vector<std::string> result;
	
	std::string name = "";
	std::string query = "select name from Lists;";
	sqlite3_stmt *stmt;
	const char *pzTail;
	int errcode = sqlite3_prepare(db, query.c_str(), query.length(), &stmt, &pzTail);
	if(SQLITE_OK != errcode) {
		DEBUG_OUT("Failed to prepare statement: " << sqlite3_errstr(errcode));
		return std::vector<std::string>();
	}
	errcode = sqlite3_step(stmt);
	if((SQLITE_DONE != errcode)&&(SQLITE_ROW != errcode)) {
		DEBUG_OUT("sqlite3_step failed: " << sqlite3_errstr(errcode) << " / " << errcode);
		return std::vector<std::string>();
	}
	while(SQLITE_TEXT == sqlite3_column_type(stmt, 0)) {
		result.push_back(
				std::string(
					reinterpret_cast<const char*>(sqlite3_column_text(stmt, 0))
					)
				);
		errcode = sqlite3_step(stmt);
		if((SQLITE_DONE != errcode)&&(SQLITE_ROW != errcode)) {
			DEBUG_OUT("sqlite3_step failed: " << sqlite3_errstr(errcode) << " / " << errcode);
			return result;
		}
	}

	sqlite3_finalize(stmt);	
	return result;
}

void
DataProvider::removeList(std::string const &listName) {
	std::string query = "delete from Lists where name = $1;";
	sqlite3_stmt *stmt;
	const char *pzTail;
	int errcode = sqlite3_prepare(db, query.c_str(), query.length(), &stmt, &pzTail);
	if(SQLITE_OK != errcode) {
		DEBUG_OUT("Failed to prepare statement: " << sqlite3_errstr(errcode));
		return;
	}
	errcode = sqlite3_bind_text(stmt, 1, listName.c_str(), listName.length(), SQLITE_STATIC);
	if(SQLITE_OK != errcode) {
		DEBUG_OUT("Failed to bind text to statement: " << sqlite3_errstr(errcode));
		return;
	}
	errcode = sqlite3_step(stmt);
	if(SQLITE_DONE != errcode) {
		DEBUG_OUT("sqlite3_step failed: " << sqlite3_errstr(errcode));
		return;
	}
	sqlite3_finalize(stmt);
}
