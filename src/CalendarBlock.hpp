#ifndef __HAVE_CALENDARBLOCK_HPP__
#define __HAVE_CALENDARBLOCK_HPP__
class CalendarBlock;

#include <gtkmm.h>
#include <glibmm/ustring.h>
#include "Date.hpp"
#include <vector>
#include <memory>

//
//	CalendarBlock
//
// Uses TextView-s to show notes for each weekday
// TextView-s are initialized in linkTextViews()
// * set to not accept Tab characters
// * set to wrap words, chars

class CalendarBlock : public Gtk::Container {
	protected:
		Glib::RefPtr<Gtk::Builder> builder;
		
		struct Weekday {
			Gtk::Label *label;
			Gtk::TextView *textView;
			Date date;
			Glib::ustring loadedNote;
			Gtk::Overlay *overlay;
			Gtk::Image *statusIcon;
		};
		
		Weekday weekdays[7];
		
		Gtk::Button *buttonPrevWeek;
		Gtk::Button *buttonNextWeek;
		
		unsigned int firstDayOfWeek = 7;
		Date thisWeeksFirstDay;
	public:
		CalendarBlock(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);
		~CalendarBlock();
		
		void onDailyNoteLoaded(void *agdnStruct);
		
		void notifyAboutExit();
	private:
		void initWeekdays();
		void linkButtons();
		void weekReinit(char isInit = false);
		
		void saveWeek();
		void saveWeekday(Date d);
	protected:
		void onPrevWeekClicked();
		void onNextWeekClicked();
};

#endif // __HAVE_CALENDARBLOCK_HPP__
