#include "SettingsProvider.hpp"
#include <map>
#include <fstream>

#include "../ext/cpp/INIReader.h"
#include "../ext/StreamProxy.hpp"

#include "debug.hpp"
#include "version.h"

#if __linux__
#include <stdlib.h>
#endif

SettingsProvider& SettingsProvider::getInstance()
{
	static SettingsProvider instance; // initialized only once
	return instance;
}

SettingsProvider::SettingsProvider()
{
	setConfigDefaults();
#ifdef DEBUG
	auto search = defaults.find("config-file");
	DEBUG_OUT("Config file: " << search->second);
#endif
}

SettingsProvider::~SettingsProvider()
{
	
}

void SettingsProvider::loadSettings() throw (std::string)
{
	// here we suppose that there's no way "config-file" is left
	// undefined (see setConfigDefaults()

	DEBUG_OUT("opening file: '" << getParam("config-file") << "'");
	INIReader reader(getParam("config-file"));
	if (reader.ParseError() < 0) {
		throw(EXCEPTION_STR("file not found : " << getParam("config-file")));
	}

	for(const char* key : configurationItems)
	{
		std::string val = reader.Get("configuration", key, "");

		if(0==val.length())
		{
			DEBUG_OUT("Key " << key << " undefined");
			continue;
		}

		// Remove quotation marks
		if('\"' == val.front())
		{
			val.erase(0,1);
		}
		if('\"' == val.back())
		{
			val.pop_back();
		}

		configuration[key] = val;

		DEBUG_OUT("Load key: " << key << "=" << val);
	}
}

std::string
SettingsProvider::getParam(const std::string& name) throw (std::string)
{
	// check overrides first
	auto overridesVal = overrides.find(name);
	if (overridesVal != overrides.end()) {
		return overridesVal->second;
	}
	// check configuration then
	auto configVal = configuration.find(name);
	if (configVal != configuration.end()) {
		return configVal->second;
	}
	// at last: defaults
	auto defaultsVal = defaults.find(name);
	if(defaultsVal != defaults.end()) {
		return defaultsVal->second;
	}
	// throw
	throw(EXCEPTION_STR("name=" << name << ", param not found"));
}

void
SettingsProvider::overrideParam(const std::string& name, const std::string& value)
{
	overrides[name] = value;
}

void SettingsProvider::setConfigDefaults()
{
#ifdef DEBUG_PATHS
	defaults["config-file"]= "./config.ini";
	defaults["uifile"] = "./ui/ui.glade";

	#warning Debug configuration:	./config.ini
	#warning Debug UI file:			./ui/ui.glade
#elif __linux__
	// XDG standard: XDG_CONFIG_HOME or ~/.config/
	const char* HOME = getenv("HOME");
	const char* XDG_CONFIG_HOME = getenv("XDG_CONFIG_HOME");
	const char* XDG_DATA_HOME = getenv("XDG_DATA_HOME");
	std::string config_dir, data_dir;
	if(NULL != XDG_CONFIG_HOME)
	{
		config_dir = std::string(StreamProxy() << XDG_CONFIG_HOME << "/" << ApplicationInfo.app_name);
	} else {
		config_dir = std::string(StreamProxy() << HOME << "/.config/" << ApplicationInfo.app_name);
	}
	if(NULL != XDG_DATA_HOME)
	{
		data_dir = std::string(StreamProxy() << XDG_DATA_HOME << "/" << ApplicationInfo.app_name);
	} else {
		data_dir = std::string(StreamProxy() << HOME << "/.local/share/" << ApplicationInfo.app_name);
	}

	defaults["config-file"]= config_dir + "/config.ini";
	defaults["uifile"] = data_dir + "/ui.glade";
	defaults["database"] = data_dir + "/db.db";

	#warning Default configuration:	~/.config/Planner/config.ini
	#warning Default UI file:		~/.local/share/Planner/ui.glade
#else
	#warning "Platform is unknown"
	#error "No platform-specific options in SettingsProvider::setConfigDefaults()"
#endif
}
