#ifndef __HAVE_DATAPROVIDER_HPP__
#define __HAVE_DATAPROVIDER_HPP__
class DataProvider;

#include "Date.hpp"
#include <vector>
#include <sqlite3.h>

class DataProvider
{
	public: // Singleton stuff
		static DataProvider& getInstance();
		DataProvider(DataProvider const&)			= delete;
		void operator=(DataProvider const&)		= delete;
		
	public:
		DataProvider();
		virtual ~DataProvider();
		
		void init() throw (std::string);
		
		// Daily notes
		void saveDailyNote(Date d, std::string const& notes);
		std::string getDailyNote(Date d);
		void removeDailyNote(Date d);
		// Lists
		void saveList(std::string const& listName, std::string const& listText);
		std::string getList(std::string const& listName);
		void removeList(std::string const& listName);
		
		std::vector<std::string> getListNames();

		
	protected:
	private:
		sqlite3 *db;
		
		// vvv check that tables exist, otherwise create them
		void checkInitDB() throw(const char*);
};

#endif // __HAVE_DATAPROVIDER_HPP__
