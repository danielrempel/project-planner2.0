#ifndef __HAVE_SETTINGSPROVIDER_HPP__
#define __HAVE_SETTINGSPROVIDER_HPP__
class SettingsProvider;

#include <string>
#include <map>
#include <vector>

class SettingsProvider
{
	public: // Singleton stuff
		static SettingsProvider& getInstance();
		SettingsProvider(SettingsProvider const&)				= delete;
		void operator=(SettingsProvider const&)					= delete;

	public:
		SettingsProvider();
		virtual ~SettingsProvider();
		
		void loadSettings() throw (std::string);
			// insert values to configuration
		
		std::string getParam(const std::string& name) throw (std::string);
			// check overrides, then configuraion, then defaults
		
		void overrideParam(const std::string& name, const std::string& value);
			// inserts a value into overrides
		
	protected:
	private:
		void setConfigDefaults();
		
		std::map<std::string, std::string> defaults;
		std::map<std::string, std::string> configuration;
		std::map<std::string, std::string> overrides;
		
		std::vector<const char*> configurationItems = { "database", "firstDayOfWeek", "uifile" };
		
		// config.ini:
		// [configuration] ; the only section
		// database = ""
		// firstDayOfWeek = 7 ; Sunday
		
		// Partially done:
		//  * has defaults hardcoded
		//  * able to read ini file
		//  * unable to parse command line args
};

#endif // __HAVE_SETTINGSPROVIDER_HPP__
