#ifndef __HAVE_DATE_HPP__
#define __HAVE_DATE_HPP__
class Date;

#include <glibmm/ustring.h>

#include <chrono>

#include "../ext/date.h"
#include "../ext/iso_week.h"

using namespace date;
using namespace iso_week;

using days = std::chrono::duration
    <int, std::ratio_multiply<std::ratio<24>, std::chrono::hours::period>>;

class Date
{
	// TODO: place appropriate const-s
	public:
		Date(sys_days date);
		Date(const Date& date);
		Date(); // current date
		~Date();
		
		int getStorageDate();
		sys_days getSysDays() const;
		unsigned int getWeekday();
		
		std::string getString();
		Glib::ustring getUString();
		
		operator std::string();
		operator Glib::ustring();
		
		Date operator+(const days& b);
		Date operator+(const int& b);
		Date operator-(const int& b);
		
		bool operator==(const Date &b) const;

		/*std::string describe() const;*/

	protected:
	private:
		sys_days date;
};

#endif // __HAVE_DATE_HPP__
