#ifndef __HAVE_DEBUG_HPP__
#define __HAVE_DEBUG_HPP__

#ifdef DEBUG
#include <iostream>
#include "../ext/StreamProxy.hpp"
#define DEBUG_OUT(X) \
	do { std::cout << __PRETTY_FUNCTION__ << ": " << X << std::endl;} while (0);

#define EXCEPTION_STR(X) \
	std::string(StreamProxy() << __PRETTY_FUNCTION__ << ": " << X)
#else
#define DEBUG_OUT(X)
#define EXCEPTION_STR(X)
#endif


#endif
