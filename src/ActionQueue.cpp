#include "ActionQueue.hpp"

#include "debug.hpp"

#include <chrono> // for wait_for lock

ActionQueue& ActionQueue::getInstance()
{
	static ActionQueue instance; // initialized once only
	return instance;
}

ActionQueue::ActionQueue()
 : running(1), ownThread(&ActionQueue::run, this)
{
}

ActionQueue::~ActionQueue()
{
}

void
ActionQueue::addAction(Action* a)
{
	queueMutex.lock();
	queue.push(a);
	queueMutex.unlock();
	std::unique_lock<std::mutex> lck(cvMutex);
    cv.notify_one();
    DEBUG_OUT("action added");
}

// not the most elegant implementation but
// still a working one
void
ActionQueue::run()
{
	DEBUG_OUT("starting execution");
	bool queueEmpty;
	runningMutex.lock();
	char isRunning = running;
	runningMutex.unlock();
	while(1 == isRunning)
	{
		
		//DEBUG_OUT("running");
		
		queueMutex.lock();
		queueEmpty = queue.empty();
		//DEBUG_OUT("queueEmpty: " << queueEmpty << "; queue size: " << queue.size());
		queueMutex.unlock();
		
		while(!queueEmpty)
		{
			queueMutex.lock();
			queue.front()->execute();
			queue.pop();
			queueEmpty = queue.empty();
			//DEBUG_OUT("queueEmpty: " << queueEmpty << "; queue size: " << queue.size());
			queueMutex.unlock();
		}

		{
			//DEBUG_OUT("queue processed, sleeping");
			std::unique_lock<std::mutex> lck(cvMutex);
			cv.wait_for(lck, std::chrono::milliseconds(500));
		}

		runningMutex.lock();
		queueMutex.lock();
			queueEmpty = queue.empty();
			isRunning = running | (!queueEmpty);
		queueMutex.unlock();
		runningMutex.unlock();
	}
	DEBUG_OUT("ending execution");
}

void
ActionQueue::stop()
{
	runningMutex.lock();
	running = 0;
	runningMutex.unlock();
	std::unique_lock<std::mutex> lck(cvMutex);
    cv.notify_one();
    DEBUG_OUT("stop signal sent");
}

void
ActionQueue::join()
{
	DEBUG_OUT("joining the thread");
	ownThread.join();
	DEBUG_OUT("AQ fully stopped");
}
