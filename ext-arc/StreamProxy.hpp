// © 2011 Nawaz <http://stackoverflow.com/users/415784/nawaz>
// Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)
// look: CCBYSA3.0 License / https://creativecommons.org/licenses/by-sa/3.0/legalcode

#ifndef __HAVE__STREAMPROXY_HPP__
#define __HAVE__STREAMPROXY_HPP__

#include <sstream>

struct StreamProxy
{
   std::stringstream ss;
   template<typename T>
   StreamProxy & operator << (const T &data)
   {
        ss << data;
        return *this;
   }
   operator std::string() { return ss.str(); }
};

#endif
