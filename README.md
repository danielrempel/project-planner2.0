 * Makefile is @mbcrawfo's GenericMakefile, MIT
 * ext/date.h, ext/iso_week.h is @HowardHinnant's date, MIT
 * ext/StreamProxy.hpp is Nawaz@stackoverflow 's code, CCBYSA-3.0
 * ext/ini.c-h, ext/cpp/INIReader.cpp-hpp - inih by benhoyt@github, BSD 3-Clause

Build:

 * extract date, StreamProxy and inih to ext/ library preserving relative
    paths (e.g. inih/cpp/INIReader.cpp -> ext/cpp/INIReader.cpp)
 * make

![](screenshot.png)

