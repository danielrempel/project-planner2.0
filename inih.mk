%.o: %.c
	@echo "Compiling: $< -> $@"
	$(CMD_PREFIX)gcc -Wall -Wpedantic -c $< -o $@
%.o: cpp/%.cpp
	@echo "Compiling: $< -> $@"
	$(CMD_PREFIX)g++ -Wall -Wpedantic -c $< -o $@

libinih.a: ini.o INIReader.o
	@echo "Linking: $@"
	$(CMD_PREFIX)ar -cq $@ ini.o INIReader.o

distclean:
	@-rm *.o

clean:
	@-rm *.o *.a

.PHONY: distclean clean
